import { delay } from 'redux-saga';

import { put } from 'redux-saga/effects';

export default function* handleApiError(type, error, { reset = false } = {}) {
  yield put({ type, payload: { error } });
  if (reset) {
    yield delay(2000);
    yield put({ type, payload: { error: null } });
  }
}
