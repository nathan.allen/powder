export default class ExpiredTokenError extends Error {
  name = 'TokenExpiredError';

  message = 'Your session has expired. Please login again.';
}
